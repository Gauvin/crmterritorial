---
title: "SpatiallyVaryingSmoother"
author: "Charles"
date: "12/02/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Libraries

```{r setup, include=FALSE}

library(dplyr)
library(magrittr)
library(purrr)
library(plotly)
library(reshape2)
library(tidyverse)

```

# Local files

```{r setup, include=FALSE}

source(here::here(file.path("R","surfaceBuildingFuns.R")))
source(here::here(file.path("R","gaussianKernelFuns.R")))
source(here::here(file.path("R","nadarayaWatsonFuns.R")))

```


## Discontinuous (piecewise constant) function of x and y

```{r}

resultInitSurf <- buildSurface(disc2DFct)
resultBumpySurf <- buildSurface(disc2DFctBumpier)

```

```{r}
pInitSurf <- plot_ly(z = ~resultInitSurf$matSurf) %>% add_surface()
(pInitSurf)
```
```{r}
pBumpySurf <- plot_ly(z = ~resultBumpySurf$matSurf) %>% add_surface( )  
(pBumpySurf)
htmlwidgets::saveWidget(as_widget(pBumpySurf), "BumpySurfRaw.html")
```

```{r}

smoothResultsGaussKernel1 <- smoothNadWatsonNDFct(resultBumpySurf$dfLongSurf, gaussianKernelSigma1, "gaussianSigma1NDKernelBumpy")
smoothResultsGaussKernel1$matSmoothed
smoothResultsGaussKernel1$dfLongSurf

smoothResultsGaussKernel1$dfLongSurf %>% select(contains("diff")) %>% summary

```


```{r}

pBumpySurfSmoothed <- plot_ly(z = ~smoothResultsGaussKernel1$matSmoothed) %>% add_surface()
(pBumpySurfSmoothed)
htmlwidgets::saveWidget(as_widget(pBumpySurfSmoothed), "BumpySurfSmoothed.html")
```

#Set the variance
```{r}

dfLongSurf <- getVarDummyFun(resultBumpySurf$dfLongSurf)

smoothResultsGaussKernel1SpatialVar <- smoothNadWatsonNDFctSpatialVar(dfLongSurf, "gaussianSigma1NDKernelBumpySpatialVar")
smoothResultsGaussKernel1SpatialVar$matSmoothed
smoothResultsGaussKernel1SpatialVar$dfLongSurf

```
```{r }

pBumpySurfSmoothedSpatialVar <- plot_ly(z = ~smoothResultsGaussKernel1SpatialVar$matSmoothed) %>% add_surface()
(pBumpySurfSmoothedSpatialVar)
htmlwidgets::saveWidget(as_widget(pBumpySurfSmoothedSpatialVar), "BumpySurfSmoothedSpatialVar.html")

```
